/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author nati2
 */
public class Files {
    public Boolean userExist(String userIdentifier, String passwordInsertedByUser) {
        boolean result = false;
        File entityFile = new File("users.txt");
        try {
            BufferedReader entity = new BufferedReader(new FileReader(entityFile));
            ArrayList<String> entityArray = new ArrayList<>();
            while (entity.ready()) {
                entityArray.add(entity.readLine());
            }

            for (String singleEntity : entityArray) {
                String[] tempArray = singleEntity.split(",", 0);
                if (tempArray[3].equals(passwordInsertedByUser) && tempArray[0].equals(userIdentifier)) {
                    result = true;
                }
            }
            entity.close();
        } catch (Exception ex) {
            System.out.println("Error al leer archivo: users.txt\n" + ex);
        }
        return result;
    }
    
    public boolean theValueAlreadyExists(String fileName, String userIdentifier){
        boolean result = false;
        File entityFile = new File(fileName);
        try {
            BufferedReader entity = new BufferedReader(new FileReader(entityFile));
            ArrayList<String> entityArray = new ArrayList<>();
            while (entity.ready()) {
                entityArray.add(entity.readLine());
            }

            for (String singleEntity : entityArray) {
                String[] tempArray = singleEntity.split(",", 0);
                if (tempArray[0].equals(userIdentifier)) {
                    result = true;
                }
            }
            entity.close();
        } catch (Exception ex) {
            System.out.println("Error al leer archivo: users.txt\n" + ex);
        }
        return result;
    }

    public Boolean isAdmin(String userIdentifier) {
        boolean result = false;
        File entityFile = new File("users.txt");
        try {
            BufferedReader entity = new BufferedReader(new FileReader(entityFile));
            ArrayList<String> entityArray = new ArrayList<>();
            while (entity.ready()) {
                entityArray.add(entity.readLine());
            }
            for (String singleEntity : entityArray) {
                String[] tempArray = singleEntity.split(",", 0);
                if (tempArray[0].equals(userIdentifier)) {
                    if (tempArray[4].equals("0")) {
                        result = true;
                    }
                }
            }
            entity.close();
        } catch (Exception ex) {
            System.out.println("Error al leer archivo: users.txt\n" + ex);
        }
        return result;
    }
    
    public ArrayList<String> readingFiles(String fileName) {
        File fileInstance = new File(fileName);
        ArrayList<String> fileContent = new ArrayList<>();
        ArrayList<String> valuesFiltered = new ArrayList<>();
        try {
            BufferedReader file = new BufferedReader(new FileReader(fileInstance));
            while (file.ready()) {
                fileContent.add(file.readLine());
            }
            for (String content : fileContent) {               
                String[] tempArray = content.split(",", 0);
                valuesFiltered.add(tempArray[1]);
            }
            file.close();
        } catch (Exception ex) {
            System.out.println("Error al leer archivo: " + fileName + ".txt\n" + ex);
        }
        return valuesFiltered;
    }
    
    public String createFlight(int airlineIndex,
                               int flightPrice,
                               Date parsedDepartureDateTime,
                               String departureAirport,
                               Date parsedArrivalDateTime,
                               String arrivalAirport,
                               String flightDuration,
                               String planeID,
                               String pilotID,
                               String clientServiceID){
        File flightsFile = new File("flights.txt");
        File seatsFile = new File("seats.txt");
        String index = gettingIndexFromFile("flights.txt");
        String parsedAirlineIndex = Integer.toString(airlineIndex + 1);
        String parsedFlightPrice = Integer.toString(flightPrice);
        String parsedToStringDepartureDateTime = parsedDepartureDateTime.toString();
        String parsedToStringArrivalDateTime = parsedArrivalDateTime.toString();
        String result = "";
            
        if(index.equals("")){
            index = "1";
        }
        try {
            flightsFile.createNewFile();
        }catch (Exception ex) {
            System.out.println("Error al crear el archivo flights.txt:\n" + ex);
        }

        try{

            BufferedWriter save = new BufferedWriter(new FileWriter(flightsFile, true));
            
            save.write(
                    index + "," +
                    parsedAirlineIndex + "," +
                    parsedFlightPrice + "," +
                    parsedToStringDepartureDateTime + "," +
                    departureAirport + "," +
                    parsedToStringArrivalDateTime + "," +
                    arrivalAirport + "," +
                    flightDuration + "," +
                    planeID + "," +
                    pilotID + "," +
                    clientServiceID + "\n"
            );
            result = "Vuelo insertado satisfactoriamente.";
            save.close();
        }catch (Exception ex) {
            System.out.println("Error al guardar el archivo flights:\n" + ex);
            result = "Error al guardar el archivo flights";
        }  
        
        try {
            seatsFile.createNewFile();
        }catch (Exception ex) {
            System.out.println("Error al crear el archivo flights.txt:\n" + ex);
        }
        
        try{
            BufferedWriter save = new BufferedWriter(new FileWriter(seatsFile, true));
            save.write(
                    index + "-Z,01-A,02-C,03-A,04-A,05-C,06-A,07-A,08-C,09-A,10-A,11-C,12-A,13-A,14-C,15-A,16-A,17-C,18-A,19-A,20-C,21-A,22-A,23-C,24-A,25-A,26-C,27-A,28-A,29-C,30-T,31-A,32-C,33-A,34-T,35-T,36-T" 
                    );
            save.close();
        }catch (Exception ex) {
            System.out.println("Error al guardar el archivo Seats:\n" + ex);
            result = "Error al guardar el archivo Seats";
        }       
        return result;
    }
    
    public String registerPassengerUser(String userID , String userName, int userAge, String userPassword){
        File usersFile = new File("users.txt");
        String userType = "1";
        String result = "";
        try {
            usersFile.createNewFile();
        }catch (Exception ex) {
            System.out.println("Error al crear el archivo users:\n" + ex);
        }

        try{
            BufferedWriter saveUser = new BufferedWriter(new FileWriter(usersFile, true));
            saveUser.write("\r\n" + userID + "," + userName + "," + userAge + "," + userPassword + "," + userType);
            result = "Usuario registrado.";
            saveUser.close();
        }catch (Exception ex) {
            System.out.println("Error al guardar el archivo users:\n" + ex);
        }
        
        return result;
    }
    
    
    
    public String gettingIndexFromFile(String fileName) {
        File fileInstance = new File(fileName);
        ArrayList<String> fileContent = new ArrayList<>();
        String result = "";
        try {
            BufferedReader file = new BufferedReader(new FileReader(fileInstance));
            while (file.ready()) {
                fileContent.add(file.readLine());
            }
            result = Integer.toString(fileContent.size() + 1);
            file.close();
        } catch (Exception ex) {
            System.out.println("Error al leer archivo: " + fileName + ".txt\n" + ex);
        }
        return result;
    }
    
    public ArrayList<String> readingFlightsFile(String fileName) {
        File fileInstance = new File(fileName);
        ArrayList<String> fileContent = new ArrayList<>();
        ArrayList<String> valuesFiltered = new ArrayList<>();
        try {
            BufferedReader file = new BufferedReader(new FileReader(fileInstance));
            while (file.ready()) {
                fileContent.add(file.readLine());
            }
            for (String content : fileContent) {               
                String[] tempArray = content.split(",", 0);
                valuesFiltered.add(fileContent.toString());
            }
            file.close();
        } catch (Exception ex) {
            System.out.println("Error al leer archivo: " + fileName + ".txt\n" + ex);
        }
        return valuesFiltered;
    }
    
    public String getValueById(String fileName, String index){
        File entityFile = new File(fileName);
        String result = "";
        
        try {
            BufferedReader entity = new BufferedReader(new FileReader(entityFile));
            ArrayList<String> entityArray = new ArrayList<>();
            while (entity.ready()) {
                entityArray.add(entity.readLine());
            }
            for (String singleEntity : entityArray) {
                String[] tempArray = singleEntity.split(",", 0);              
                if(tempArray[0].equals(index)){
                    result = tempArray[1];                  
                } 
            }
            entity.close();
        } catch (Exception ex) {
            System.out.println("Error al leer archivo: " + fileName + ".txt\n" + ex);
        }
        return result;
    }
    
    public String getIdByValue(String fileName, String value, int indexOfValue){
        File entityFile = new File(fileName);
        String result = "";
        
        try {
            BufferedReader entity = new BufferedReader(new FileReader(entityFile));
            ArrayList<String> entityArray = new ArrayList<>();
            while (entity.ready()) {
                entityArray.add(entity.readLine());
            }
            for (String singleEntity : entityArray) {
                String[] tempArray = singleEntity.split(",", 0);              
                if(tempArray[indexOfValue].equals(value)){
                    result = tempArray[0];                  
                } 
            }
            entity.close();
        } catch (Exception ex) {
            System.out.println("Error al leer archivo: " + fileName + ".txt\n" + ex);
        }
        return result;
    }
    
    public ArrayList<String> getAvailableFlights(String departureAirport, String arrivalAirport, String stopoverAirport, Date departureDate){
        DateFormat mediumFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.getDefault());  //Sirve para convertir Date a String en un formato especifico  
        DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);      
        ArrayList<String>  availableFlights = new ArrayList<String>();     
        File entityFile = new File("flights.txt");
        String parsedDepartureDateUserInput = mediumFormat.format(departureDate);  
        String parsedDepartureDateFileValue = "";
        Date departureDateFromFile = null;
        String airline = ""; 
        
        try {
            BufferedReader entity = new BufferedReader(new FileReader(entityFile));
            ArrayList<String> entityArray = new ArrayList<>();
            while (entity.ready()) {
                entityArray.add(entity.readLine());
            }
            for (String singleEntity : entityArray) { 
                String[] tempArray = singleEntity.split(",", 0); 
                departureDateFromFile = dateFormat.parse(tempArray[3]);
                parsedDepartureDateFileValue = mediumFormat.format(departureDateFromFile);            
                if(departureAirport.equals(tempArray[4]) && arrivalAirport.equals(tempArray[6]) && parsedDepartureDateUserInput.equals(parsedDepartureDateFileValue)){
                    airline = getValueById("airlines.txt" , tempArray[1]);                   
                    availableFlights.add("Aerolinea: " + airline + "," + "Código de Avión: " + tempArray[8]);
                }/*if(departureAirport.equals(tempArray[4]) && parsedDepartureDateUserInput.equals(parsedDepartureDateFileValue)){// Trato de buscar el depart airport que haga match
                    stopoverAirport = tempArray[6];
                    if(stopoverAirport.equals(tempArray[4]))
                    
                    airline = getValueById("airlines.txt" , tempArray[1]);                   
                    availableFlights.add("Aerolinea: " + airline + "," + "Código de Avión: " + tempArray[8]);
                }*/

            }
            entity.close();
        } catch (Exception ex) {
            System.out.println("Error al leer archivo: aircrew.txt\n" + ex);
        }       
        return availableFlights;
    }
    
    
    public String[] getFlightDetails(String departureAirport , String arrivalAirport, int amountOfPassengers, Date departureDate, String availableFlightsInnerValue){
        DateFormat mediumFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.getDefault());  //Sirve para convertir Date a String en un formato especifico  
        DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
        DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
        
        String[] flightDetails = new String[9]; 
        String parsedDepartureDateUserInput = mediumFormat.format(departureDate);  
        String parsedDepartureDateFileValue = "";
        Date departureDateFromFile = null;
        Date arrivalDateFromFile = null;
        File entityFile = new File("flights.txt");
        String airlineValue = "";
        String airlineId = "";  
        String departureTime = "";
        String arrivalTime = "";
        String totalPrice = "";
        int flightPrice = 0;
            
        String[] separatedByTwoPoints = availableFlightsInnerValue.split(": " , 0);
        String[] separatedByComma = separatedByTwoPoints[1].split("," , 0);
        airlineValue = separatedByComma[0];
        airlineId = getIdByValue("airlines.txt" , airlineValue, 1);
      
        try {
            BufferedReader entity = new BufferedReader(new FileReader(entityFile));
            ArrayList<String> entityArray = new ArrayList<>();
            while (entity.ready()) {
                entityArray.add(entity.readLine());
            }
            for (String singleEntity : entityArray) { 
                String[] tempArray = singleEntity.split(",", 0);              
                departureDateFromFile = dateFormat.parse(tempArray[3]);
                arrivalDateFromFile = dateFormat.parse(tempArray[5]);              
                parsedDepartureDateFileValue = mediumFormat.format(departureDateFromFile);
                
                if(departureAirport.equals(tempArray[4]) && 
                        arrivalAirport.equals(tempArray[6]) && 
                        parsedDepartureDateUserInput.equals(parsedDepartureDateFileValue) &&
                        airlineId.equals(tempArray[1])){
                    departureTime  = hourFormat.format(departureDateFromFile);
                    arrivalTime = hourFormat.format(arrivalDateFromFile);
                    flightPrice = Integer.parseInt(tempArray[2]);                  
                    totalPrice = Integer.toString(amountOfPassengers * flightPrice);
                    
                    flightDetails[0] = airlineValue;
                    flightDetails[1] = departureAirport;
                    flightDetails[2] = arrivalAirport;
                    flightDetails[3] = departureTime;
                    flightDetails[4] = arrivalTime;
                    flightDetails[5] = tempArray[7];
                    flightDetails[6] = tempArray[2];
                    flightDetails[7] = totalPrice; 
                    flightDetails[8] = tempArray[0]; //retornando el index del vuelo
                }             
            }
            entity.close();
        } catch (Exception ex) {
            System.out.println("Error al leer archivo: flights.txt\n" + ex);
        }        
        return flightDetails;
    }
    
    public String[] checkAvailability(int airlineIndex){
        String[] returnedValuesAvailability = new String[2];
        File entityFile = new File("aircrew.txt");
        String airlineCode =  Integer.toString(airlineIndex + 1);
        
        try {
            BufferedReader entity = new BufferedReader(new FileReader(entityFile));
            ArrayList<String> entityArray = new ArrayList<>();
            while (entity.ready()) {
                entityArray.add(entity.readLine());
            }
            for (String singleEntity : entityArray) {
                String[] tempArray = singleEntity.split(",", 0);
                if(tempArray[2].equals(airlineCode)){//worker is part of this airline
                    String val = "";
                    if(tempArray[3].equals("1")){//User is pilot                     
                        if(tempArray[4].equals("1")){ //worker is available
                            returnedValuesAvailability[0] = "1";
                        }              
                    }else{ //User is client service
                        if(tempArray[4].equals("1")){ //worker is available
                            returnedValuesAvailability[1] = "1";
                        }                    
                    }
                } 
            }
            
            if(returnedValuesAvailability[1] == null){
                returnedValuesAvailability[1] = "0";
            }
            if(returnedValuesAvailability[0] == null){
                returnedValuesAvailability[0] = "0";
            }   
            entity.close();
        } catch (Exception ex) {
            System.out.println("Error al leer archivo: aircrew.txt\n" + ex);
        }
        
        return returnedValuesAvailability;
    }
    
    public String getEntityIdFromPlanes(int airlineIndex){
        File entityFile = new File("planes.txt");
        String airlineCode =  Integer.toString(airlineIndex + 1);
        String result = "";
        
        try {
            BufferedReader entity = new BufferedReader(new FileReader(entityFile));
            ArrayList<String> entityArray = new ArrayList<>();
            while (entity.ready()) {
                entityArray.add(entity.readLine());
            }
            for (String singleEntity : entityArray) {
                String[] tempArray = singleEntity.split(",", 0);
                
                if(tempArray[1].equals(airlineCode)){
                    if(tempArray[3].equals("1")){
                        result = tempArray[0];//returns Ids  
                    }                    
                } 
            }
            entity.close();
        } catch (Exception ex) {
            System.out.println("Error al leer archivo: planes.txt\n" + ex);
        }
        return result;
    }
    
    public String getEntityIdFromAirCrew(int airlineIndex, String employeeRoleValue){
        //airlineIndex, "aircrew.txt", 2, 4, 3
        File entityFile = new File("aircrew.txt");
        String airlineCode =  Integer.toString(airlineIndex + 1);
        String result = "";
        
        try {
            BufferedReader entity = new BufferedReader(new FileReader(entityFile));
            ArrayList<String> entityArray = new ArrayList<>();
            while (entity.ready()) {
                entityArray.add(entity.readLine());
            }
            for (String singleEntity : entityArray) {
                String[] tempArray = singleEntity.split(",", 0);
                
                if(tempArray[2].equals(airlineCode)){ //Validates airline code
                    if(tempArray[4].equals("1")){ //availability
                        if(tempArray[3].equals(employeeRoleValue)){ //type of employee
                            result = tempArray[0];//returns Ids  
                        }                     
                    }                    
                } 
            }
            entity.close();
        } catch (Exception ex) {
            System.out.println("Error al leer archivo: aircrew.txt\n" + ex);
        }
        return result;
    }
}
