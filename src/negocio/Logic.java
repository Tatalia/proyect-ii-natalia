/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import datos.Files;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author nati2
 */
public class Logic {

    Files files = new Files();

    public Boolean ifTheUserExists(String userIdentifier, String passwordInsertedByUser) {
        return files.userExist(userIdentifier, passwordInsertedByUser);
    }
    
    public Boolean theValueAlreadyExists(String fileName, String userIdentifier) {
        return files.theValueAlreadyExists(fileName, userIdentifier);
    }

    public Boolean isAdmin(String userIdentifier) {
        return files.isAdmin(userIdentifier);
    }

    public ArrayList<String> insertingFile(String filename) {
        return files.readingFiles(filename);
    }
    
    public ArrayList<String> readingFlightsFile(String filename) {
        return files.readingFlightsFile(filename);
    }

    public String createFlight(int airlineIndex,
                                int flightPrice,
                                Date parsedDepartureDateTime,
                                String departureAirport,
                                Date parsedArrivalDateTime,
                                String arrivalAirport,
                                String flightDuration,
                                String planeID,
                                String pilotID,
                                String clientServiceID) {

        return files.createFlight(airlineIndex,
                                    flightPrice,
                                    parsedDepartureDateTime,
                                    departureAirport,
                                    parsedArrivalDateTime,
                                    arrivalAirport,
                                    flightDuration,
                                    planeID,
                                    pilotID,
                                    clientServiceID);
    }
    
    public String registerNewUser(String userID , String userName, int userAge, String userPassword){
        return files.registerPassengerUser(userID, userName, userAge, userPassword);
    }

    public ArrayList<String> getAvailableFlights(String departureAirport, String arrivalAirport, String stopoverAirport, Date departureDate) {
        return files.getAvailableFlights(departureAirport, arrivalAirport, stopoverAirport, departureDate);
    }
    
    public String[] getFlightDetails(String departureAirport , String arrivalAirport, int amountOfPassengers, Date departureDate, String availableFlightsInnerValue){
        return files.getFlightDetails(departureAirport, arrivalAirport, amountOfPassengers, departureDate, availableFlightsInnerValue);
    }
    
}
