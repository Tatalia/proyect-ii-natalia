/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.ArrayList;


/**
 *
 * @author nati2
 */

public class PassengersLogic {
    boolean thereAreAvailableSeats = false;

    public String updateFlightSeats(String indexOfFlight, int numberOfPassengers) {
        File entityFile = new File("seats.txt");
        String result = "";   
        String updatedEntity = "";
       
        try {
            BufferedReader entity = new BufferedReader(new FileReader(entityFile));
            ArrayList<String> entityArray = new ArrayList<>();
            while (entity.ready()) {
                entityArray.add(entity.readLine());
            }
            for (String singleEntity : entityArray) {//divide el documento en lineas
                String[] tempArray = singleEntity.split(",", 0);//divide la linea por comas
                if(tempArray[0].equals(indexOfFlight)){//vuelo encontrado
                    updatedEntity = getUpdatedEntity(tempArray , numberOfPassengers);
                }
            }
            entity.close();//cerramos el reader para poder implementar el edit
            if(thereAreAvailableSeats){
                result = updateSeatFile(entityArray, updatedEntity, indexOfFlight);
            }else{
                result = "No hay asientos disponibles :(";
            }        
        } catch (Exception ex) {
            System.out.println("Error al leer archivo: seats.txt\n" + ex);
        }
        return result;
    }
    
    public String getUpdatedEntity(String[] singleEntityArray, int numberOfPassengers){
        boolean thereIsSomeoneInTheFliht = false;
        boolean wasAlreadySeparated = false;    
        String[] cloneArray = singleEntityArray;
        String result = "";
       
        for(int i = 0; i < singleEntityArray.length; i++ ){
            String seat = singleEntityArray[i];
            String[] seatIndividualArray = seat.split("-", 0); 
            if(seatIndividualArray[1].replace(" ", "").equals("O")){
                thereIsSomeoneInTheFliht = true;
                i = singleEntityArray.length;
            }           
        }

        for(int u = 1; u <= numberOfPassengers; u++){//itera por cant de pasajeros
            for(int i = 0; i <= singleEntityArray.length; i++ ){//recorro arreglo de asientos por asientos disponibles
                String seat = singleEntityArray[i];
                String[] seatIndividualArray = seat.split("-", 0); 
                if(!seatIndividualArray[1].replace(" ", "").equals("O") && 
                    !seatIndividualArray[1].replace(" ", "").equals("Z") && 
                    !seatIndividualArray[1].replace(" ", "").equals("X") && 
                    !seatIndividualArray[1].replace(" ", "").equals("T")){
                    
                    if(thereIsSomeoneInTheFliht == true && wasAlreadySeparated == false){
                        singleEntityArray[i] = seatIndividualArray[0] + "-X";
                        cloneArray[i] = seatIndividualArray[0] + "-X";
                        wasAlreadySeparated = true;
                    }else{
                        thereAreAvailableSeats = true;
                        singleEntityArray[i] = seatIndividualArray[0] + "-O";//encuentro asiento y lo pongo en ocupado
                        cloneArray[i] = seatIndividualArray[0] + "-O";
                        i = singleEntityArray.length;
                    }                   
                }           
            }
        } 
        //creating a string from an Array
        for(int i = 0; i < cloneArray.length; i++ ){
            result += cloneArray[i]+",";        
        }
        return result;
    }
    
    public String updateSeatFile(ArrayList<String> entityArray, String singleEntity, String index){
        String result = "";
        File entityFile = new File("seats.txt");
        String filePath = entityFile.getAbsolutePath();

        try {
            Files.delete(Paths.get(filePath));
        } catch (NoSuchFileException x) {
            System.err.format("%s: no such" + " file or directory%n");
            result = "Error al insertar información al archivo";
        } catch (DirectoryNotEmptyException x) {
            System.err.format("%s not empty%n", filePath);
            result = "Error al insertar información al archivo";
        } catch (IOException x) {
            // File permission problems are caught here.
            System.err.println(x);
            result = "Error al insertar información al archivo";
        }

        try { 
            entityFile.createNewFile();
        } catch (Exception ex) {
            System.out.println("Error al crear el archivo:\n" + ex);
            result = "Error al insertar información al archivo";
        }

        for (String flight : entityArray) {
            String[] tempArray = flight.split(",", 0);
            if(tempArray[0].equals(index)){
                try {
                    BufferedWriter jobs = new BufferedWriter(new FileWriter(entityFile, true));
                    jobs.write(singleEntity + "\r\n");
                    jobs.close();
                    result = "Asientos Asignados satisfactoriamente!!";
                } catch (Exception ex) {
                    System.out.println("Error al insertar información al archivo:\n" + ex);
                    result = "Error al insertar información al archivo";
                }
            }else{  
                try {
                    BufferedWriter jobs = new BufferedWriter(new FileWriter(entityFile, true));
                    jobs.write(flight + "\r\n");
                    jobs.close();
                    result = "Asientos Asignados satisfactoriamente!!";
                } catch (Exception ex) {
                    System.out.println("Error al insertar información al archivo:\n" + ex);
                    result = "Error al insertar información al archivo";
                }
            } 
        }                     
        return result;
    }   
}
